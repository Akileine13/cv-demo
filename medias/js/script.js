$(function () {

    // Scroll mooth
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });

    // Dark mode
    $('.fa-moon').on('click', function () {
        let theme = $('#theme');
        if ($("i").hasClass("fa-moon")) {
            Cookies.set("theme", "dark", { expires: 7 });
            if (Cookies.get("theme") === 'dark') {
                theme.attr('href', 'medias/css/dark.css');
                $('.fa-moon').removeClass('fa-moon').addClass('fa-sun');
            }
        } else {
            Cookies.set("theme", "light", { expires: 7 });
            if (Cookies.get("theme") === 'light') {
                theme.attr('href', 'medias/css/style.css');
                $('.fa-sun').removeClass('fa-sun').addClass('fa-moon');
            }
        }
    });


    // Init animation
    AOS.init();
});